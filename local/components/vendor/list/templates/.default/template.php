<?php

use Vendor\Components\ListSimple;

/**
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var ListSimple $component
 * @var string $componentPath
 */
foreach ($component->getErrorCollection() as $error) {
    ShowError($error->getMessage());
}

echo '<table width="100%">';

//show thead
$tmpl = str_repeat('<th>%s</th>', count($component->getItems()[0]));
/** @noinspection PhpFormatFunctionParametersMismatchInspection */
echo sprintf("<tr>$tmpl</tr>", ...array_values(array_keys($component->getItems()[0])));

//show tbody
foreach ($component->getItems() as $arItem) {
    $tmpl = str_repeat('<td>%s</td>', count($arItem));
    /** @noinspection PhpFormatFunctionParametersMismatchInspection */
    echo sprintf("<tr>$tmpl</tr>", ...array_values($arItem));
}

echo '</table>';