<?php
/**
 *
 */

namespace Vendor\Components;

use Bitrix\Main\ORM\Query\Query;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\UserTable;
use CBitrixComponent;
use Exception;
use Vendor\Component\Traits\UseErrorCollectionTrait;
use Vendor\Exporter\CsvExport;
use Vendor\Exporter\ExcelExport;
use Vendor\Exporter\XmlExport;

/** @noinspection AutoloadingIssuesInspection */

/**
 * Class UserList
 * @package Vendor\Components
 */
class UserList extends CBitrixComponent
{
    use UseErrorCollectionTrait;

    const ALLOWED_MODE = ['list', 'csv', 'excel', 'xml'];

    /** @var PageNavigation */
    private $nav;

    /**
     * @param $arParams
     *
     * @return array
     */
    public function onPrepareComponentParams($arParams): array
    {
        $arParams['COUNT'] = (int)$arParams['COUNT'];
        $arParams['COUNT'] = $arParams['COUNT'] ?: 10;
        $arParams['MODE'] = htmlspecialcharsbx($this->request->get('mode'));
        $arParams['MODE'] = in_array($arParams['MODE'], self::ALLOWED_MODE, true) ? $arParams['MODE'] : 'list';

        return $arParams;
    }

    /**
     * @return void
     */
    public function executeComponent()
    {
        $this->getItems();
        $this->handleMode();

        if (!$this->getErrorCollection()->isEmpty()) {
            $this->abortResultCache();
        }
        $this->includeComponentTemplate();
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        /** @var Query $query */
        if ($this->arResult['ITEMS'] !== null) {
            return $this->arResult['ITEMS'];
        }

        $this->arResult['ITEMS'] = [];

        try {
            $query = UserTable::query();
            $query->setSelect(['ID', 'EMAIL', 'NAME'])
                  ->addOrder('ID');

            if ($this->arParams['MODE'] === 'list') {
                $query->setLimit($this->getNavigation()->getLimit())
                      ->setOffset($this->getNavigation()->getOffset())
                      ->countTotal(true);

                $rs = $query->exec();
                $this->getNavigation()->setRecordCount($rs->getCount());
            } else {
                $rs = $query->exec();
            }

            $this->arResult['ITEMS'] = $rs->fetchAll();
        } catch (Exception $e) {
            $this->addError($e->getMessage());
        }

        return $this->arResult['ITEMS'];
    }

    /**
     * @return PageNavigation
     */
    public function getNavigation(): PageNavigation
    {
        if ($this->nav instanceof PageNavigation) {
            return $this->nav;
        }

        $this->nav = new PageNavigation('nav-user');
        $this->nav->setPageSize($this->arParams['COUNT']);
        $this->nav->initFromUri();

        return $this->nav;
    }

    /**
     * @return UserList
     */
    private function handleMode(): self
    {
        if ($this->arParams['MODE'] === 'list') {
            return $this;
        }
        try {
            if ($this->arParams['MODE'] === 'csv') {
                (new CsvExport($this->getItems()))->exec()->toOutput();
            }

            if ($this->arParams['MODE'] === 'excel') {
                (new ExcelExport($this->getItems()))->exec()->toOutput();
            }

            if ($this->arParams['MODE'] === 'xml') {
                (new XmlExport($this->getItems()))->exec()->toOutput();
            }

        } catch (Exception $e) {
            $this->addError($e->getMessage());
        }

        return $this;
    }
}