<?php

namespace Vendor\Exporter;

/**
 * Class Exporter
 * @package Vendor\Exporter
 */
abstract class Exporter
{
    /**
     * @var string
     */
    protected $data;

    /**
     * @var array
     */
    protected $items;

    /**
     * Exporter constructor.
     *
     * @param array $arItems
     */
    public function __construct(array $arItems)
    {
        $this->items = $arItems;
        $this->data = '';
    }

    /** @return static */
    abstract public function exec();

    abstract public function toOutput();

    abstract public function toFile(string $fileName);
}