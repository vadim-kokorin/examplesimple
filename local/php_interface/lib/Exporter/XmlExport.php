<?php

namespace Vendor\Exporter;

use Bitrix\Main\Application;
use Bitrix\Main\NotImplementedException;
use Bitrix\Main\XmlWriter;
use RuntimeException;

class XmlExport extends Exporter
{
    const FILE_PATH = '/upload/tmp/user-list.xml';

    /**
     * @var XmlWriter
     */
    protected $xmlWriter;

    /**
     * @return self
     */
    public function exec(): self
    {
        if (!$this->items) {
            return $this;
        }

        $this->xmlWriter = new XmlWriter(
            [
                'file'        => self::FILE_PATH,
                'create_file' => true,
                'charset'     => SITE_CHARSET,
                'lowercase'   => true //приводить ли все теги к нижнему регистру
            ]
        );

        $this->xmlWriter->openFile();

        $this->xmlWriter->writeBeginTag('items');
        foreach ($this->items as $arItem) {
            $this->xmlWriter->writeItem($arItem, 'item');
        }
        $this->xmlWriter->writeEndTag('items');

        $this->xmlWriter->closeFile();

        if ($this->xmlWriter->getErrors()) {
            throw new RuntimeException(implode(', ', $this->xmlWriter->getErrors()));
        }

        return $this;
    }

    /**
     * Отдать контент в браузер
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\SystemException
     */
    public function toOutput()
    {
        global $APPLICATION;

        $filePath = Application::getDocumentRoot() . self::FILE_PATH;
        $APPLICATION->RestartBuffer();
        $response = Application::getInstance()->getContext()->getResponse();
        $response->addHeader('Content-Type', 'application/xml')
                 ->addHeader('Content-Disposition', 'attachment;filename=user-list.xml');

        echo file_get_contents($filePath);

        require $_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/main/include/epilog_after.php';

        die();
    }

    /**
     * @param string $fileName
     *
     * @throws NotImplementedException
     */
    public function toFile(string $fileName)
    {
        throw new NotImplementedException('Не реализовано');
    }
}